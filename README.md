# MyuC

## Instruction set

### 0000 - nop

    No operation

### 10yx - add x, y

    Add without Carry
    r[x] = r[x] + r[y]

### 11yx - adc x, y

    Add with Carry
    r[x] = r[x] + r[y] + C

### 12yx - sub x, y

    Substract without Carry
    r[x] = r[x] - r[y]

### 13yx - sbc x, y

    Substract with Carry
    r[x] = r[x] - r[y] - C

### 14yx - cmp x, y

    Compare without Carry
    r[x] - r[y]

### 15yx - cpc x, y

    Compare with Carry
    r[x] - r[y]

### 16yx - and x, y

    Logical AND
    r[x] = r[x] & r[y]

### 17yx - or x, y

    Logical OR
    r[x] = r[x] | r[y]

### 18yx - xor x, y

    Logical XOR
    r[x] = r[x] ^ r[y]

### 19yx - lsl x, y

    Logical Shift Left
    r[x] = r[x] << r[y]

### 1Ayx - lsr x, y

    Logical Shift Right
    r[x] = r[x] >> r[y]

### 010x - neg x

    Two's complement
    r[x] = $0 - r[x]

### 020x - com x

    One's complement
    r[x] = $FF - r[x]

### 030x - tst x

    Test for Zero or Minus
    r[x] = r[x] & r[x]

### 040x - clr x

    Clear Register
    r[x] = r[x] ^ r[x]

### 050x - ser x

    Clear Register
    r[x] = $FFFF

### 060x - inc x

    Increment Register
    r[x] = r[x] + 1

### 070x - dec x

    Decrement Register
    r[x] = r[x] - 1

### 8kkx - ldil x, kk

    Load Immediate Low byte
    r[x](7:0) = kk

### 9kkx - ldih x, kk

    Load Immediate High byte
    r[x](7:0) = kk

### Akyx - ld x, y, k

    Load Indirect with displacement from Data memory
    r[x] = DATA(r[y] + k)

### Bkyx - st x, y, k

    Store Indirect with displacement from Data memory
    DATA(r[x] + k) = r[y]

### Ckyx - lpm x, y, k

    Load Indirect with displacement from Program Memory
    r[x] = PROG(r[x] + k)

### Dkkx - in x, k

    In from I/O Location
    r[x] = IO(k)

### Ekkx - out k, x

    Out from I/O Location
    IO(k) = r[x]

### Fkk0 - bra kk

    Branch Always
    PC <- PC + kk

### Fkk1 - breq kk

    Branch if Equal
    if (Z = 1) then PC <- PC + kk

### Fkk2 - brne kk

    Branch if Not Equal
    if (Z = 0) then PC <- PC + kk

### Fkk3 - brlo kk

    Branch if Lower
    if (C = 1) then PC <- PC + kk

### Fkk4 - brsh kk

    Branch if Same or Higher
    if (C = 0) then PC <- PC + kk

### Fkk5 - brmi kk

    Branch if Minus
    if (N = 1) then PC <- PC + kk

### Fkk6 - brpl kk

    Branch if Plus
    if (N = 0) then PC <- PC + kk

### Fkk7 - brlt kk

    Branch if Lower Than (signed)
    if (N ^ V  = 1) then PC <- PC + kk

### Fkk8 - brge kk

    Branch if Greater or Equal Than (signed)
    if (N ^ V  = 0) then PC <- PC + kk

### 080x - jmp x

    Indirect jump
    PC = r[x]
