module alu
  (clk,
   rst,
   a, b, op,
   fin_carry,
   fout_carry,
   fout_overflow,
   fout_zero,
   fout_neg,
   res_out);

   parameter WIDTH = 16;

   input                clk;
   input                rst;
   input [WIDTH - 1:0]  a, b;
   input [3:0]          op;
   input                fin_carry;

   output [WIDTH - 1:0] res_out;
   output               fout_carry;
   output               fout_overflow;
   output               fout_zero;
   output               fout_neg;

   wire [WIDTH - 1:0]   and_op_out;
   wire [WIDTH - 1:0]   or_op_out;
   wire [WIDTH - 1:0]   xor_op_out;
   wire [WIDTH:0]       add_op_out;
   wire [WIDTH:0]       sl_op_out;
   wire [WIDTH - 1:0]   sr_op_out;
   wire                 instr_nop;
   wire                 instr_and;
   wire                 instr_or;
   wire                 instr_xor;
   wire                 instr_add;
   wire                 instr_sub;
   wire                 instr_sl;
   wire                 instr_sr;

   reg [WIDTH - 1:0]    res;

   assign instr_nop = op == 'h0;
   assign instr_and = op == 'h1;
   assign instr_or = op == 'h2;
   assign instr_xor = op == 'h3;
   assign instr_add = op == 'h4;
   assign instr_sub = op == 'h5;
   assign instr_sl = op == 'h6;
   assign instr_sr = op == 'h7;

   assign and_op_out[WIDTH - 1:0] = a & b;
   assign or_op_out[WIDTH - 1:0] = a | b;
   assign xor_op_out[WIDTH - 1:0] = a ^ b;
   assign add_op_out[WIDTH:0] = a + (instr_sub ? -b : b) + fin_carry;
   assign sl_op_out[WIDTH:0] = a << b;
   assign sr_op_out[WIDTH - 1:0] = a >> b;

   assign fout_carry = (add_op_out[WIDTH] & instr_add)
                     | (sl_op_out[WIDTH] & instr_sl);
   assign fout_overflow = ((a[WIDTH - 1] & b[WIDTH - 1]
                            & ~res[WIDTH - 1])
                           | (~a[WIDTH - 1] & ~b[WIDTH - 1]
                              & res[WIDTH - 1]))
                          & (instr_add | instr_sub);
   assign fout_zero = res[WIDTH - 1:0] == 0;
   assign fout_neg = res[WIDTH - 1];

   assign res_out = res;

   always @(posedge clk) begin
      res <= (and_op_out & {WIDTH{instr_and}})
           | (or_op_out & {WIDTH{instr_or}})
           | (xor_op_out & {WIDTH{instr_xor}})
           | (add_op_out
              & {WIDTH{instr_add | instr_sub}})
           | (sl_op_out & {WIDTH{instr_sl}})
           | (sr_op_out & {WIDTH{instr_sr}})
           | (a & {WIDTH{instr_nop}});
   end

endmodule // alu
