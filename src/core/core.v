module core;

   parameter WIDTH = 16;
   parameter REG_ADDR_WIDTH = 4;

   reg                          clk = 0;
   reg                          rst = 1;

   wire [WIDTH - 1 : 0]         if_id_pc;
   wire [WIDTH - 1 : 0]         if_id_ir;

   wire [WIDTH - 1 : 0]         id_ex_pc;
   wire                         id_ex_branch;
   wire [3 : 0]                 id_ex_branch_cond;
   wire [REG_ADDR_WIDTH - 1:0]  id_ex_dest;
   wire [3 : 0]                 id_ex_alu_op;
   wire                         id_ex_alu_src;
   wire [WIDTH - 1 : 0]         id_ex_x;
   wire [WIDTH - 1 : 0]         id_ex_y;
   wire [WIDTH - 1 : 0]         id_ex_k;
   wire [REG_ADDR_WIDTH : 0]    id_ex_x_addr;
   wire [REG_ADDR_WIDTH : 0]    id_ex_y_addr;
   wire                         id_ex_wb_reg_write;
   wire                         id_ex_wb_hi;
   wire                         id_ex_mem_write;
   wire                         id_ex_mem_read;
   wire                         id_ex_io;
   wire                         id_ex_y_mux;

   wire [REG_ADDR_WIDTH - 1:0]  ex_mem_dest;
   wire [WIDTH - 1 : 0]         ex_mem_res;
   wire [WIDTH - 1 : 0]         ex_mem_write_data;
   wire                         ex_mem_wb_reg_write;
   wire                         ex_mem_wb_hi;
   wire                         ex_mem_mem_write;
   wire                         ex_mem_mem_read;
   wire                         ex_mem_io;

   wire [REG_ADDR_WIDTH - 1:0]  mem_wb_dest;
   wire                         mem_wb_wb_reg_write;
   wire                         mem_wb_wb_mem_to_reg;
   wire                         mem_wb_wb_hi;
   wire [WIDTH - 1 : 0]         mem_wb_read_data;
   wire [WIDTH - 1 : 0]         mem_wb_res;

   wire [WIDTH - 1 : 0]         pc_fb;
   wire                         br;
   wire                         br_ok;
   wire [3 : 0]                 br_cond;
   wire [1 : 0]                 forward_mux_x;
   wire [1 : 0]                 forward_mux_y;

   wire                         flag_carry;
   wire                         flag_overflow;
   wire                         flag_zero;
   wire                         flag_neg;

   reg                          sr_flag_carry;
   reg                          sr_flag_overflow;
   reg                          sr_flag_zero;
   reg                          sr_flag_neg;

   reg [WIDTH - 1 : 0]          wb_data;
   reg [REG_ADDR_WIDTH - 1 : 0] wb_addr;
   reg                          wb_enable;
   reg                          wb_hi;

   fetch fetch
     (clk,
      rst,
      pc_fb,
      br_ok,
      if_id_pc,
      if_id_ir);

   decoder decoder
     (clk,
      rst,
      if_id_pc,
      if_id_ir,
      wb_enable,
      wb_hi,
      wb_data,
      wb_addr,
      id_ex_pc,
      id_ex_branch,
      id_ex_branch_cond,
      id_ex_dest,
      id_ex_alu_op,
      id_ex_alu_src,
      id_ex_x,
      id_ex_y,
      id_ex_k,
      id_ex_x_addr,
      id_ex_y_addr,
      id_ex_wb_reg_write,
      id_ex_wb_hi,
      id_ex_mem_write,
      id_ex_mem_read,
      id_ex_io,
      id_ex_y_mux);

   exec exec
     (clk,
      rst,
      id_ex_pc,
      id_ex_branch,
      id_ex_branch_cond,
      id_ex_dest,
      id_ex_alu_op,
      id_ex_alu_src,
      id_ex_x,
      id_ex_y,
      id_ex_k,
      id_ex_wb_reg_write,
      id_ex_wb_hi,
      id_ex_mem_write,
      id_ex_mem_read,
      id_ex_io,
      id_ex_y_mux,
      sr_flag_carry,
      forward_mux_x,
      forward_mux_y,
      ex_mem_res,
      mem_wb_res,
      pc_fb,
      br,
      br_cond,
      ex_mem_dest,
      flag_carry,
      flag_overflow,
      flag_zero,
      flag_neg,
      ex_mem_res,
      ex_mem_write_data,
      ex_mem_wb_reg_write,
      ex_mem_wb_hi,
      ex_mem_mem_write,
      ex_mem_mem_read,
      ex_mem_io);

   memory memory
     (clk,
      rst,
      ex_mem_dest,
      ex_mem_res,
      ex_mem_write_data,
      ex_mem_wb_reg_write,
      ex_mem_wb_hi,
      ex_mem_mem_write,
      ex_mem_mem_read,
      ex_mem_io,
      mem_wb_dest,
      mem_wb_read_data,
      mem_wb_res,
      mem_wb_wb_reg_write,
      mem_wb_wb_hi,
      mem_wb_wb_mem_to_reg);

   forwarding_unit forwarding_unit
     (id_ex_wb_reg_write,
      id_ex_x_addr,
      id_ex_y_addr,
      ex_mem_wb_reg_write,
      ex_mem_dest,
      mem_wb_wb_reg_write,
      mem_wb_dest,
      forward_mux_x,
      forward_mux_y);

   branch_unit branch_unit
     (br, br_cond,
      sr_flag_carry,
      sr_flag_overflow,
      sr_flag_zero,
      sr_flag_neg,
      br_ok);

   integer              i;
   initial
     begin
        $dumpfile("myuc.vcd");
        $dumpvars(0,
                  clk,
                  rst,
                  if_id_pc,
                  if_id_ir,
                  id_ex_pc,
                  id_ex_branch,
                  id_ex_dest,
                  id_ex_alu_op,
                  id_ex_alu_src,
                  id_ex_x,
                  id_ex_y,
                  id_ex_k,
                  id_ex_x_addr,
                  id_ex_y_addr,
                  id_ex_wb_reg_write,
                  id_ex_wb_hi,
                  id_ex_mem_write,
                  id_ex_mem_read,
                  id_ex_io,
                  id_ex_y_mux,
                  ex_mem_dest,
                  ex_mem_res,
                  ex_mem_write_data,
                  ex_mem_wb_reg_write,
                  ex_mem_wb_hi,
                  ex_mem_mem_write,
                  ex_mem_mem_read,
                  ex_mem_io,
                  mem_wb_dest,
                  mem_wb_wb_reg_write,
                  mem_wb_wb_hi,
                  mem_wb_wb_mem_to_reg,
                  mem_wb_read_data,
                  mem_wb_res,
                  pc_fb,
                  br,
                  br_cond,
                  br_ok,
                  forward_mux_x,
                  forward_mux_y,
                  flag_carry,
                  flag_overflow,
                  flag_zero,
                  flag_neg);
        #1;
        clk <= 1;
        #1;
        clk <= 0;
        rst <= 0;
        #1;
        for (i = 128; i > 0; i = i - 1)
          begin
             clk <= 1;
             #1;
             clk <= 0;
             #1;
          end
     end // initial begin

   always @(posedge clk) begin
      wb_data <= mem_wb_wb_mem_to_reg ? mem_wb_read_data : mem_wb_res;
      wb_addr <= mem_wb_dest;
      wb_enable <= mem_wb_wb_reg_write;
      wb_hi <= mem_wb_wb_hi;
      sr_flag_carry <= flag_carry;
      sr_flag_overflow <= flag_overflow;
      sr_flag_zero <= flag_zero;
      sr_flag_neg <= flag_neg;
   end

endmodule // core
