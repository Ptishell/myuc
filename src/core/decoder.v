module decoder
  (clk,
   rst,
   pc_in,
   instr,
   reg_write_enable,
   reg_write_hi,
   reg_write_data,
   reg_write_addr,
   pc_out,
   branch_out,
   branch_cond_out,
   dest_out,
   alu_op_out,
   alu_src_out,
   x, y, k_out,
   x_addr, y_addr,
   wb_reg_write_out,
   wb_hi_out,
   mem_write_out,
   mem_read_out,
   io_out,
   y_mux_out);

   parameter WIDTH = 16;
   parameter ADDR_WIDTH = 4;
   parameter HEIGHT = 2 ** ADDR_WIDTH;

   input                        clk;
   input                        rst;
   input [WIDTH - 1 : 0]        pc_in;
   input [WIDTH - 1 : 0]        instr;
   input                        reg_write_enable;
   input                        reg_write_hi;
   input [WIDTH - 1 : 0]        reg_write_data;
   input [ADDR_WIDTH - 1 : 0]   reg_write_addr;

   output [WIDTH - 1 : 0]       pc_out;
   output                       branch_out;
   output [3:0]                 branch_cond_out;
   output [ADDR_WIDTH - 1 : 0]  dest_out;
   output [3 : 0]               alu_op_out;
   output                       alu_src_out;
   output [WIDTH - 1 : 0]       x;
   output [WIDTH - 1 : 0]       y;
   output [WIDTH - 1 : 0]       k_out;
   output [ADDR_WIDTH : 0]      x_addr;
   output [ADDR_WIDTH : 0]      y_addr;
   output                       wb_reg_write_out;
   output                       wb_hi_out;
   output                       mem_write_out;
   output                       mem_read_out;
   output                       io_out;
   output                       y_mux_out;

   wire                         instr_add;
   wire                         instr_adc;
   wire                         instr_sub;
   wire                         instr_sbc;
   wire                         instr_cmp;
   wire                         instr_cpc;
   wire                         instr_and;
   wire                         instr_or;
   wire                         instr_xor;
   wire                         instr_lsl;
   wire                         instr_lsr;
   wire                         instr_neg;
   wire                         instr_com;
   wire                         instr_tst;
   wire                         instr_clr;
   wire                         instr_ser;
   wire                         instr_inc;
   wire                         instr_dec;
   wire                         instr_ldil;
   wire                         instr_ldih;
   wire                         instr_ld;
   wire                         instr_st;
   wire                         instr_lpm;
   wire                         instr_in;
   wire                         instr_out;
   wire                         instr_bra;
   wire                         instr_breq;
   wire                         instr_brne;
   wire                         instr_brlo;
   wire                         instr_brsh;
   wire                         instr_brmi;
   wire                         instr_brpl;
   wire                         instr_brlt;
   wire                         instr_brge;
   wire                         instr_jmp;

   reg [WIDTH - 1 : 0]          pc;
   reg                          br;
   reg [3:0]                    br_cond;
   reg [ADDR_WIDTH - 1 : 0]     dest;
   reg [3 : 0]                  alu_op;
   reg                          alu_src;
   reg [WIDTH - 1 : 0]          k;

   reg [ADDR_WIDTH : 0]         rd_addr_x;
   reg [ADDR_WIDTH : 0]         rd_addr_y;

   reg                          wb_reg_write;
   reg                          wb_hi;
   reg                          mem_write;
   reg                          mem_read;
   reg                          io;
   reg                          y_mux;

   assign instr_add  = instr[15:8]  == 'h10;
   assign instr_adc  = instr[15:8]  == 'h11;
   assign instr_sub  = instr[15:8]  == 'h12;
   assign instr_sbc  = instr[15:8]  == 'h13;
   assign instr_cmp  = instr[15:8]  == 'h14;
   assign instr_cpc  = instr[15:8]  == 'h15;
   assign instr_and  = instr[15:8]  == 'h16;
   assign instr_or   = instr[15:8]  == 'h17;
   assign instr_xor  = instr[15:8]  == 'h18;
   assign instr_lsl  = instr[15:8]  == 'h19;
   assign instr_lsr  = instr[15:8]  == 'h1A;
   assign instr_neg  = instr[15:4]  == 'h010;
   assign instr_com  = instr[15:4]  == 'h020;
   assign instr_tst  = instr[15:4]  == 'h030;
   assign instr_clr  = instr[15:4]  == 'h040;
   assign instr_ser  = instr[15:4]  == 'h050;
   assign instr_inc  = instr[15:4]  == 'h060;
   assign instr_dec  = instr[15:4]  == 'h070;
   assign instr_ldil = instr[15:12] == 'h8;
   assign instr_ldih = instr[15:12] == 'h9;
   assign instr_ld   = instr[15:12] == 'hA;
   assign instr_st   = instr[15:12] == 'hB;
   assign instr_lpm  = instr[15:12] == 'hC;
   assign instr_in   = instr[15:12] == 'hD;
   assign instr_out  = instr[15:12] == 'hE;
   assign instr_bra  = instr[15:12] == 'hF && instr[3:0] == 'h0;
   assign instr_breq = instr[15:12] == 'hF && instr[3:0] == 'h1;
   assign instr_brne = instr[15:12] == 'hF && instr[3:0] == 'h2;
   assign instr_brlo = instr[15:12] == 'hF && instr[3:0] == 'h3;
   assign instr_brsh = instr[15:12] == 'hF && instr[3:0] == 'h4;
   assign instr_brmi = instr[15:12] == 'hF && instr[3:0] == 'h5;
   assign instr_brpl = instr[15:12] == 'hF && instr[3:0] == 'h6;
   assign instr_brlt = instr[15:12] == 'hF && instr[3:0] == 'h7;
   assign instr_brge = instr[15:12] == 'hF && instr[3:0] == 'h8;
   assign instr_jmp  = instr[15:4]  == 'h080;

   assign k_out = k;
   assign wb_reg_write_out = wb_reg_write;
   assign wb_hi_out = wb_hi;
   assign mem_write_out = mem_write;
   assign mem_read_out = mem_read;
   assign io_out = io;
   assign y_mux_out = y_mux;

   assign pc_out = pc;
   assign branch_out = br;
   assign branch_cond_out = br_cond;
   assign dest_out = dest;
   assign alu_op_out = alu_op;
   assign alu_src_out = alu_src;
   assign x_addr = rd_addr_x;
   assign y_addr = rd_addr_y;

   reg_file reg_file(clk,
                     rst,
                     reg_write_enable,
                     reg_write_hi,
                     reg_write_addr,
                     reg_write_data,
                     rd_addr_x[ADDR_WIDTH - 1 : 0],
                     rd_addr_y[ADDR_WIDTH - 1 : 0],
                     x, y);

   always @(posedge clk) begin
      if (rst)
        begin
           pc <= 0;
           rd_addr_x <= 0;
           rd_addr_y <= 0;
           dest <= 0;
           wb_reg_write <= 0;
           wb_hi <= 0;
           mem_write <= 0;
           mem_read <= 0;
           io <= 0;
           alu_op <= 0;
           alu_src <= 0;
           k <= 0;
           br <= 0;
           br_cond <= 0;
        end
      else
        begin
           pc <= pc_in;
           rd_addr_x[ADDR_WIDTH - 1:0] <= instr[3 : 0];
           rd_addr_y[ADDR_WIDTH - 1:0] <= instr[7 : 4];
           rd_addr_x[ADDR_WIDTH] <= instr_add
                                    | instr_adc
                                    | instr_sub
                                    | instr_sbc
                                    | instr_cmp
                                    | instr_cpc
                                    | instr_and
                                    | instr_or
                                    | instr_xor
                                    | instr_lsl
                                    | instr_lsr
                                    | instr_neg
                                    | instr_com
                                    | instr_tst
                                    | instr_clr
                                    | instr_ser
                                    | instr_inc
                                    | instr_dec
                                    | instr_ldil
                                    | instr_ldih
                                    | instr_ld
                                    | instr_st
                                    | instr_lpm
                                    | instr_in
                                    | instr_out
                                    | instr_jmp;
           rd_addr_y[ADDR_WIDTH] <= instr_add
                                    | instr_adc
                                    | instr_sub
                                    | instr_sbc
                                    | instr_cmp
                                    | instr_cpc
                                    | instr_and
                                    | instr_or
                                    | instr_xor
                                    | instr_lsl
                                    | instr_lsr
                                    | instr_ld
                                    | instr_st
                                    | instr_lpm
                                    | instr_in
                                    | instr_out;
           dest <= instr[3 : 0];
           wb_reg_write <= instr_add
                           | instr_adc
                           | instr_sub
                           | instr_sbc
                           | instr_and
                           | instr_or
                           | instr_xor
                           | instr_lsl
                           | instr_lsr
                           | instr_neg
                           | instr_com
                           | instr_clr
                           | instr_ser
                           | instr_inc
                           | instr_dec
                           | instr_ldil
                           | instr_ldih
                           | instr_ld
                           | instr_lpm
                           | instr_in;
           wb_hi <= instr_ldih;
           mem_write <= instr_st | instr_out;
           mem_read <= instr_ld | instr_in;
           io <= instr_in | instr_out;
           y_mux <= instr_out;
           alu_op <= ({3{instr_and | instr_tst | instr_inc}} & 'h1)
                  | ({3{instr_or}} & 'h2)
                  | ({3{instr_xor | instr_clr}} & 'h3)
                  | ({3{instr_add | instr_adc}} & 'h4)
                  | ({3{instr_sub | instr_sbc | instr_cmp | instr_cpc
                       | instr_neg | instr_com | instr_dec}} & 'h5)
                  | ({3{instr_lsl}} & 'h6)
                  | ({3{instr_lsr}} & 'h7);
           alu_src <= instr_ldil | instr_ldih;
           k <= ({WIDTH{instr_ldil
                        | instr_ldih
                        | instr_in
                        | instr_out
                        | instr_bra
                        | instr_breq
                        | instr_brne
                        | instr_brlo
                        | instr_brsh
                        | instr_brmi
                        | instr_brpl
                        | instr_brlt
                        | instr_brge}} & {{8{instr[11]}}, instr[11 : 4]})
             | ({WIDTH{instr_ld
                       | instr_st
                       | instr_lpm}} & {{12{instr[11]}}, instr[11 : 8]});
           br <= instr_bra
                 | instr_breq
                 | instr_brne
                 | instr_brlo
                 | instr_brsh
                 | instr_brmi
                 | instr_brpl
                 | instr_brlt
                 | instr_brge;
           br_cond <= instr[3 : 0];
        end
   end

endmodule // decoder
