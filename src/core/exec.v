module exec
  (clk,
   rst,
   pc_in,
   branch_in,
   branch_cond_in,
   dest_in,
   alu_op,
   alu_src,
   x_in, y_in, k,
   reg_write_in,
   hi_in,
   mem_write_in,
   mem_read_in,
   io_in,
   y_mux,
   fin_carry,
   forward_mux_x,
   forward_mux_y,
   forward_ex_mem,
   forward_mem_wb,
   pc_out,
   branch_out,
   branch_cond_out,
   dest_out,
   fout_carry,
   fout_overflow,
   fout_zero,
   fout_neg,
   res,
   y_out,
   reg_write_out,
   hi_out,
   mem_write_out,
   mem_read_out,
   io_out);

   parameter WIDTH = 16;
   parameter REG_WIDTH = 16;
   parameter REG_ADDR_WIDTH = 4;

   input                        clk;
   input                        rst;
   input [WIDTH - 1 : 0]        pc_in;
   input                        branch_in;
   input [3 : 0]                branch_cond_in;
   input [REG_ADDR_WIDTH - 1:0] dest_in;
   input [3 : 0]                alu_op;
   input                        alu_src;
   input [REG_WIDTH - 1 : 0]    x_in;
   input [REG_WIDTH - 1 : 0]    y_in;
   input [REG_WIDTH - 1 : 0]    k;
   input                        reg_write_in;
   input                        hi_in;
   input                        mem_write_in;
   input                        mem_read_in;
   input                        io_in;
   input                        y_mux;
   input                        fin_carry;
   input [1:0]                  forward_mux_x;
   input [1:0]                  forward_mux_y;
   input [WIDTH - 1 : 0]        forward_ex_mem;
   input [WIDTH - 1 : 0]        forward_mem_wb;

   output [WIDTH - 1 : 0]       pc_out;
   output                       branch_out;
   output [3 : 0]               branch_cond_out;
   output [REG_ADDR_WIDTH-1:0]  dest_out;
   output                       fout_carry;
   output                       fout_overflow;
   output                       fout_zero;
   output                       fout_neg;
   output [REG_WIDTH - 1 : 0]   res;
   output [REG_WIDTH - 1 : 0]   y_out;
   output                       reg_write_out;
   output                       hi_out;
   output                       mem_write_out;
   output                       mem_read_out;
   output                       io_out;

   wire [REG_WIDTH - 1 : 0]     alu_input;
   wire [REG_WIDTH - 1 : 0]     alu_x;
   wire [REG_WIDTH - 1 : 0]     alu_y;

   reg [WIDTH - 1 : 0]          pc;
   reg                          br;
   reg [3 : 0]                  br_cond;
   reg [REG_ADDR_WIDTH - 1:0]   dest;
   reg [REG_WIDTH - 1 : 0]      y;
   reg                          reg_write;
   reg                          hi;
   reg                          mem_write;
   reg                          mem_read;
   reg                          io;

   alu alu
     (clk, rst,
      alu_x, alu_y, alu_op,
      fin_carry,
      fout_carry,
      fout_overflow,
      fout_zero,
      fout_neg,
      res);

   assign pc_out = pc;
   assign branch_out = br;
   assign branch_cond_out = br_cond;
   assign dest_out = dest;
   assign y_out = y;
   assign reg_write_out = reg_write;
   assign hi_out = hi;
   assign mem_write_out = mem_write;
   assign mem_read_out = mem_read;
   assign io_out = io;
   assign alu_input = forward_mux_x == 2 ? forward_mem_wb
                  : (forward_mux_x == 1 ? forward_ex_mem : x_in);
   assign alu_x = alu_src ? k : alu_input;
   assign alu_y = forward_mux_y == 2 ? forward_mem_wb
                  : (forward_mux_y == 1 ? forward_ex_mem : y_in);

   always @(posedge clk) begin
      if (rst)
        begin
           pc <= 0;
           dest <= 0;
           y <= 0;
           reg_write <= 0;
           hi <= 0;
           mem_write <= 0;
           mem_read <= 0;
           io <= 0;
           br <= 0;
           br_cond <= 0;
        end
      else
        begin
           pc <= pc_in + k;
           dest <= dest_in;
           y <= y_mux ? k : y_in;
           reg_write <= reg_write_in;
           hi <= hi_in;
           mem_write <= mem_write_in;
           mem_read <= mem_read_in;
           io <= io_in;
           br <= branch_in;
           br_cond <= branch_cond_in;
        end
   end

endmodule // exec
