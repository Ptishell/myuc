module fetch
  (clk,
   rst,
   pc_in,
   pc_mux,
   pc_out,
   instr_out);

   parameter WIDTH = 16;
   parameter HEIGHT = 64;

   input                        clk;
   input                        rst;
   input [WIDTH - 1 : 0]        pc_in;
   input                        pc_mux;

   output [WIDTH - 1 : 0]       pc_out;
   output [WIDTH - 1 : 0]       instr_out;

   reg [WIDTH - 1 : 0]          pc = 0;
   reg [WIDTH - 1 : 0]          instr = 0;
   reg [WIDTH - 1 : 0]          prog[HEIGHT - 1:0];

   wire [WIDTH - 1 : 0]         next_pc;

   assign next_pc = pc_mux ? pc_in : pc_out;
   assign pc_out = pc + 1;
   assign instr_out = instr;

   integer i;
   always @(posedge clk) begin
      if (rst)
        begin
           pc <= 0;
           $readmemh("prog_mem.hex", prog);
        end
      else
        begin
           instr <= prog[pc];
           pc <= next_pc;
        end
   end

endmodule // fetch
