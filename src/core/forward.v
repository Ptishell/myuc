module forwarding_unit
  (id_ex_reg_write,
   id_ex_x_addr,
   id_ex_y_addr,
   ex_mem_reg_write,
   ex_mem_dest,
   mem_wb_reg_write,
   mem_wb_dest,
   forward_x,
   forward_y);

   parameter REG_ADDR_WIDTH = 4;

   input                        id_ex_reg_write;
   input [REG_ADDR_WIDTH:0]     id_ex_x_addr;
   input [REG_ADDR_WIDTH:0]     id_ex_y_addr;
   input                        ex_mem_reg_write;
   input [REG_ADDR_WIDTH - 1:0] ex_mem_dest;
   input                        mem_wb_reg_write;
   input [REG_ADDR_WIDTH - 1:0] mem_wb_dest;

   output [1:0]                 forward_x;
   output [1:0]                 forward_y;

   assign forward_x = id_ex_x_addr[REG_ADDR_WIDTH - 1 : 0] == ex_mem_dest
                      && id_ex_x_addr[REG_ADDR_WIDTH] && ex_mem_reg_write ? 1
                    : (id_ex_x_addr[REG_ADDR_WIDTH - 1 : 0] == mem_wb_dest
                       && id_ex_x_addr[REG_ADDR_WIDTH] && mem_wb_reg_write ? 2
                       : 0);

   assign forward_y = id_ex_y_addr[REG_ADDR_WIDTH - 1 : 0] == ex_mem_dest
                      && id_ex_y_addr[REG_ADDR_WIDTH] && ex_mem_reg_write ? 1
                    : (id_ex_y_addr[REG_ADDR_WIDTH - 1 : 0] == mem_wb_dest
                       && id_ex_y_addr[REG_ADDR_WIDTH] && mem_wb_reg_write ? 2
                       : 0);

endmodule // forwarding_unit
