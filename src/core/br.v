module branch_unit
  (br_in, cond,
   flag_carry,
   flag_overflow,
   flag_zero,
   flag_neg,
   br_out);

   parameter al = 'h0;
   parameter eq = 'h1;
   parameter ne = 'h2;
   parameter lo = 'h3;
   parameter sh = 'h4;
   parameter mi = 'h5;
   parameter pl = 'h6;
   parameter lt = 'h7;
   parameter ge = 'h8;

   input        br_in;
   input [3:0]  cond;
   input        flag_carry;
   input        flag_overflow;
   input        flag_zero;
   input        flag_neg;

   output       br_out;

   assign br_out = br_in &&
                   (cond == al
                    | (cond == eq && flag_zero)
                    | (cond == ne && !flag_zero)
                    | (cond == lo && flag_carry)
                    | (cond == sh && !flag_carry)
                    | (cond == mi && flag_neg)
                    | (cond == pl && !flag_neg)
                    | (cond == lt && flag_overflow ^ flag_neg)
                    | (cond == lt && !(flag_overflow ^ flag_neg)));

endmodule // branch_unit
