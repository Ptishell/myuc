module io
  (clk,
   en,
   addr,
   data_in,
   we,
   data_out);

   parameter WIDTH = 16;

   input                        clk;
   input                        en;
   input [WIDTH - 1 : 0]        addr;
   input [WIDTH - 1 : 0]        data_in;
   input                        we;

   output [WIDTH - 1 : 0]       data_out;

   reg [WIDTH - 1 : 0] data;

   assign data_out = data;

   integer             out, r;
   initial
     begin
        out = $fopenw("/dev/stdout");
     end

   always @(posedge clk)
     begin
        if (en)
          begin
             if (we)
               begin
                  case (addr)
                    'h3         : r = $fputc(data_in, out);
                    'h4         : $display("%x", data_in);
                  endcase
               end
             else
               data <= 0;
          end
     end // always @ (posedge clk)

endmodule // io
