module reg_file
  (clk,
   rst,
   we,
   wr_hi,
   wr_addr,
   wr_data,
   rd_addr_a,
   rd_addr_b,
   rd_data_a,
   rd_data_b);

   parameter WIDTH = 16;
   parameter ADDR_WIDTH = 4;
   parameter HEIGHT = 2 ** ADDR_WIDTH;

   input                        clk;
   input                        rst;
   input                        we;
   input                        wr_hi;
   input [ADDR_WIDTH - 1:0]     wr_addr;
   input [WIDTH - 1:0]          wr_data;
   input [ADDR_WIDTH - 1:0]     rd_addr_a;
   input [ADDR_WIDTH - 1:0]     rd_addr_b;

   output [WIDTH - 1:0]         rd_data_a;
   output [WIDTH - 1:0]         rd_data_b;

   reg [WIDTH - 1:0]            r[HEIGHT - 1:0];

   assign rd_data_a = we && rd_addr_a == wr_addr ? wr_data : r[rd_addr_a];
   assign rd_data_b = we && rd_addr_b == wr_addr ? wr_data : r[rd_addr_b];

   integer           i;
   always @(posedge clk) begin
      if (rst) begin
         for (i = 0; i < HEIGHT; i = i + 1) begin
           r[i] <= 0;
         end
      end else begin
         if (we)
           begin
              if (wr_hi)
                r[wr_addr] <= (wr_data << 8) | r[wr_addr];
              else
                r[wr_addr] <= wr_data;
           end
      end
   end // always @ (posedge clk)

endmodule // reg_file
