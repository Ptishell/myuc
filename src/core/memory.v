module memory
   (clk,
    rst,
    dest_in,
    addr_in,
    write_data,
    wb_reg_write_in,
    wb_hi_in,
    mem_write,
    mem_read,
    io_en,
    dest_out,
    read_data,
    addr_out,
    wb_reg_write_out,
    wb_hi_out,
    mem_to_reg_out);

   parameter WIDTH = 16;
   parameter HEIGHT = 64;
   parameter REG_ADDR_WIDTH = 4;

   input                        clk;
   input                        rst;
   input [REG_ADDR_WIDTH - 1:0] dest_in;
   input [WIDTH - 1 : 0]        addr_in;
   input [WIDTH - 1 : 0]        write_data;
   input                        wb_reg_write_in;
   input                        wb_hi_in;
   input                        mem_write;
   input                        mem_read;
   input                        io_en;

   output [REG_ADDR_WIDTH-1:0]  dest_out;
   output [WIDTH - 1 : 0]       read_data;
   output [WIDTH - 1 : 0]       addr_out;
   output                       wb_reg_write_out;
   output                       wb_hi_out;
   output                       mem_to_reg_out;

   wire [WIDTH - 1 : 0]         io_out;

   reg [WIDTH - 1 : 0]          data[HEIGHT - 1:0];
   reg [WIDTH - 1 : 0]          read_data_reg;
   reg [WIDTH - 1 : 0]          addr;
   reg [WIDTH - 1 : 0]          k;
   reg                          mem_to_reg;
   reg                          wb_reg_write;
   reg                          wb_hi;
   reg [REG_ADDR_WIDTH - 1:0]   dest;

   assign dest_out = dest;
   assign read_data = io_en ? io_out : read_data_reg;
   assign addr_out = addr;
   assign mem_to_reg_out = mem_to_reg;
   assign wb_reg_write_out = wb_reg_write;
   assign wb_hi_out = wb_hi;

   io io
     (clk,
      io_en,
      write_data,
      addr_in,
      mem_write,
      io_out);

   integer                      i;
   always @(posedge clk) begin
      if (rst)
        begin
           for (i = 0; i < HEIGHT; i = i + 1) begin
              data[i] <= 0;
           end
           read_data_reg <= 0;
           addr <= 0;
           k <= 0;
           mem_to_reg <= 0;
           wb_reg_write <= 0;
           wb_hi <= 0;
           dest <= 0;
        end
      else
        begin
           dest <= dest_in;
           if (!io_en)
             begin
                if (mem_write)
                  data[addr_in] <= write_data;
                if (mem_read)
                  read_data_reg <= data[addr_in];
             end
           addr <= addr_in;
           mem_to_reg <= mem_write;
           wb_reg_write <= wb_reg_write_in;
           wb_hi <= wb_hi_in;
        end
   end

endmodule // memory
