##
## Makefile for MyuC
##
## Made by Pierre Surply
## <pierre.surply@gmail.com>
##
## Started on  Tue Feb 25 19:16:48 2014 Pierre Surply
## Last update Mon Mar 24 16:05:00 2014 Pierre Surply
##

IV	= iverilog
IVFLAG	= -Wall
VIEW	= gtkwave

SRC	= core/alu.v		\
	  core/reg_file.v	\
	  core/fetch.v		\
	  core/decoder.v	\
	  core/exec.v		\
	  core/memory.v		\
	  core/io.v		\
	  core/forward.v	\
	  core/br.v		\
	  core/core.v
SRC	:= $(addprefix src/,$(SRC))

EXEC	= myuc

all:: $(EXEC)

simul:: $(EXEC)
	./$<

wave:: simul
	$(VIEW) ./myuc.vcd

$(EXEC): $(SRC)
	$(IV) $(IVFLAG) -s core -o $@ $^

clean::
	$(RM) $(TB)
